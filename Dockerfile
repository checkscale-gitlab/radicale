FROM debian:buster-slim

MAINTAINER Pietro Pizzo <pietro.pizzo@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

VOLUME /var/lib/radicale/collections
EXPOSE 5232
ENTRYPOINT ["radicale", "-f"]

RUN apt-get update && apt-get -y install radicale apache2-utils python3-bcrypt python3-passlib && \
    apt-get clean && rm -rf /var/cache/apt

COPY config/* /etc/radicale/
