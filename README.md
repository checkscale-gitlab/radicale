# Radicale Docker Image

## Build

Before building the image you might need to configure Radicale by editing the `config` file and adapting it to your needs.

Latest smartphones can connect to CalDav/CardDav servers only through HTTPS, so you must provide your own SSL certificate in order to make this image work. The SSL section is already filled with Let's Encrypt stuff, you only have to set the name of your domain. SSL certificates can be mounted from host but you can use an alternate path or embed the certificates into the image, it's your choice.

Before building the image you should create a `users` file as shown in the next section.

Then you can build the image as usual:

```bash
docker build -t IMAGENAME .
```

## Users

Users are stored in the `users` file which is in [htpasswd](https://httpd.apache.org/docs/current/programs/htpasswd.html) format. This file must be created in advance before building the image unless you plan to manage users in another way (i.e. as external volume).

To add a user you can issue the following command:

```bash
docker run --rm --entrypoint htpasswd radicale -nbB USERNAME PASSWORD
```

and append the output to the file. Please use the provided image to run `htpasswd` because it already has Bcrypt libraries needed for stronger security; alternatively you can run `htpasswd` anywhere provided you install the needed libraries.

## Run

You can run the image as a stand-alone Docker container (recommended only for testing and development) or deploy the image in an orchestrated cluster (i.e.: Kubernetes or Swarm). An example docker-compose file suitable for Swarm stack deploy has been provided for you, which can be used as a guide for Kubernetes deployment.

The image exports a volume where the database is stored, details in the compose file. Please adapt the volume mount to your environment.

When the container is up and running, access the web interface via HTTPS and port `5232`.

The image runs smootly on ARM 32/64bit boards.

## Additional information

For additional information, please see the official Radicale web site:

https://radicale.org/
